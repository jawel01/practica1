package com.bbva.techu.app;

import com.bbva.techu.app.entity.ProductoMongo;
import com.bbva.techu.app.entity.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductoV4Controller {
    @Autowired
    private ProductoRepository repository;

    //Listar productos
    @GetMapping(value="/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> ObtenerListado(){
        List<ProductoMongo> lista=repository.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }
    //Listar productos por nombre QUERY
    @GetMapping(value="/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> bucarPorNombre(@PathVariable String nombre){
        System.out.println("Nombre: "+nombre);
        List<ProductoMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }
    //Buscar por precio
    @GetMapping(value="/v4/productosbyprecio/{min}/{max}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> bucarPorPrecio(@PathVariable double min,@PathVariable double max ){
        System.out.println("Nombre: "+max);
        List<ProductoMongo> resultado = repository.findByPrecio(min,max);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    //Insertar producto
    @PostMapping(value="/v4/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo){
        ProductoMongo resultado = repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
    }
    //Modificar productos
    @PutMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> modiProducto(@PathVariable String id,@RequestBody ProductoMongo productoMongo){
        Optional<ProductoMongo> resultado = repository.findById(id);
        if(resultado.isPresent()){
            ProductoMongo cambio = resultado.get();
            cambio.nombre=productoMongo.nombre;
            cambio.precio=productoMongo.precio;
            cambio.color=productoMongo.color;
            ProductoMongo guardado = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado",HttpStatus.NOT_FOUND);
    }
    //Eliminar producto
    @DeleteMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> borrarProducto(@PathVariable String id){
        Optional<ProductoMongo> resultado = repository.findById(id);
        if(resultado.isPresent()){
            ProductoMongo cambio = resultado.get();
            repository.delete(cambio);
            return new ResponseEntity<String>("Producto Borrado",HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado",HttpStatus.NOT_FOUND);
    }
}
