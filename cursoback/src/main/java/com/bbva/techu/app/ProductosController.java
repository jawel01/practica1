package com.bbva.techu.app;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;


@RestController
public class ProductosController {

    private ArrayList<String> listaProeductos = null;

    public ProductosController(){
        listaProeductos = new ArrayList<>();
        listaProeductos.add("Julian Lopez");
        listaProeductos.add("Danna Lopez");
        listaProeductos.add("Carolina Lopez");
    }

    //Get lista productos
    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<String>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProeductos, HttpStatus.OK);
    }

    //Get producto especific
   /* @RequestMapping(value="/productos/{id}", method = RequestMethod.GET)
    public String obtenerProducto(@PathVariable int id){
        if(listaProeductos.size()>id){
            return listaProeductos.get(id);
        }else{
            return "no exixte id de producto ";
        }
    }*/
    //Get producto especifico

    @RequestMapping(value="/productos/{id}", method = RequestMethod.GET)
    public ResponseEntity obtenerProducto(@PathVariable int id){
        if(listaProeductos.size()>id){
            return new ResponseEntity<>(listaProeductos.get(id), HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Producto no encontrado ", HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/productos", method = RequestMethod.POST)
    public void insertar (){
        listaProeductos.add("Leonor Camargo");
    }

    @RequestMapping(value = "/productos/{nom}", method = RequestMethod.POST)
    public void insertarProducto (@PathVariable("nom") String nombre){
        listaProeductos.add(nombre);
    }

    @RequestMapping(value = "/productos/{id}/{nom}", method = RequestMethod.PUT)
    public void modificarProducto (@PathVariable("nom") String nombre,@PathVariable("id") int id ){
        if(listaProeductos.size()>id){
            listaProeductos.set(id,nombre);
        }else {
            System.out.println("No existe ID");
        }
    }

    @RequestMapping(value = "/productos/{id}", method = RequestMethod.DELETE)
    public void eliminarProducto (@PathVariable("id") int id ){
        if(listaProeductos.size()>id){
            listaProeductos.remove(id);
        }else{
            System.out.println("No existe ID");
        }

    }

    @RequestMapping(value = "/productos/", method = RequestMethod.DELETE)
    public void eliminarTodo (){
            listaProeductos.clear();
    }

}
