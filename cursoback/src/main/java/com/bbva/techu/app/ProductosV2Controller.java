package com.bbva.techu.app;

import com.bbva.techu.app.entity.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class ProductosV2Controller {

    private ArrayList<Producto> listaProductos = null;

    public ProductosV2Controller(){
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1,"Computador",150.45));
        listaProductos.add(new Producto(2,"Impresora",40.15));
        listaProductos.add(new Producto(3,"Teclado",15));
        listaProductos.add(new Producto(4,"Parlante",78.2));
    }

    //Get lista productos
    /* Get lista de productos */
    @GetMapping(value = "/V2/productos", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    //Get producto especifico

    /* Add nuevo producto */
    @PostMapping(value = "/V2/productos", produces="application/json")
    public ResponseEntity<String> addProducto() {
        System.out.println("Estoy en añadir");
        listaProductos.add(new Producto(99, "NUEVO", 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    /* Add nuevo producto con nombre */
    @PostMapping(value = "/V2/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Producto(99, "NUEVO", 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/V2/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try{
            Producto productoAModificar = listaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    @PutMapping("/v2/productos")
    public ResponseEntity<String> subirPrecio() {
        ResponseEntity<String> resultado = null;
        for (Producto p:listaProductos) {
            p.setPrecio(p.getPrecio()*1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }

    @DeleteMapping("/v2/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id){
        ResponseEntity<String> resultado = null;
        try{
            Producto productoAEliminar = listaProductos.get(id-1);
            listaProductos.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception ex){
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    @DeleteMapping("/v2/productos")
    public ResponseEntity<String> deleteProducto(){

        ResponseEntity<String> resultado = null;
        listaProductos.clear();
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }

}
