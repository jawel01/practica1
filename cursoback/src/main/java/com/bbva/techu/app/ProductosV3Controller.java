package com.bbva.techu.app;

import com.bbva.techu.app.entity.ProductoMongo;
import com.bbva.techu.app.entity.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication
public class ProductosV3Controller implements CommandLineRunner {

    @Autowired
    private ProductoRepository repository;

    public static void main (String[] args){
        SpringApplication.run(ProductosV3Controller.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Preparando Mongo DB");
        repository.deleteAll();
        repository.insert(new ProductoMongo("Carros",200.5,"Blanco"));
        repository.insert(new ProductoMongo("Computador",550.0,"Negro"));
        repository.insert(new ProductoMongo("Televisor",815.25,"Azul"));
        repository.insert(new ProductoMongo("ipad",-125.69,"Amarillo"));
        List<ProductoMongo> lista = repository.findAll();
        for(ProductoMongo p:lista){
            System.out.println(p.toString());
        }
       // repository.findByNombre("prueba");
    }
}
